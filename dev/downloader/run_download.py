import logging

from jira import JIRA

from downloader import Downloader
from issue_iterator import IssueIterator
from jira_wrapper import JiraWrapper

config = {
    "jira_username": "admin",
    "jira_password": "admin",
    "jira_url": "http://localhost:2990/jira/",
    "export_path": "export",
    "append_datetime_to_export_path": False,
    "jql": "project in (KSP, SEP) ORDER BY key ASC",
    "export_attachments": True,
    "issue_count_per_index_page": 5
}

jira = JIRA({
    "server": config['jira_url']
}, basic_auth=(config['jira_username'], config['jira_password']))

log = logging.getLogger()


def total_callback(total_issue_count):
    print(f"total_callback(), total_issue_count {total_issue_count}")


jira_wrapper = JiraWrapper(log=log, jira=jira)


def on_5_and_after_last(issue, i):
    print(f"on_5_and_after_last(), I: {i}, KEY {issue['key'] if issue else False}")


def on_3_and_after_last(issue, i):
    print(f"on_3_and_after_last(), I: {i}, KEY {issue['key'] if issue else False}")


issue_iterator = IssueIterator(log=log, jira=jira_wrapper.jira, jql=config['jql'],
                               total_callback=total_callback, page_size=50)

issue_iterator.register_on_nth_and_after_last(5, on_5_and_after_last)
issue_iterator.register_on_nth_and_after_last(3, on_3_and_after_last)

downloader = Downloader(log=log, export_dir=config['export_path'], jira_wrapper=jira_wrapper,
                        issue_iterator=issue_iterator)

downloader.download()
