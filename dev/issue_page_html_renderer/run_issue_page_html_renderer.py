import json
import logging

from field_html_renderer import FieldHtmlRenderer
from issue_page_html_renderer import IssuePageHtmlRenderer
from tools import Tools

with open('resources/rest/GET__api_2_field.json') as f:
    api_2_field = json.load(f)

api_2_field_as_map = {}
for field in api_2_field:
    api_2_field_as_map[field['id']] = field

log = logging.getLogger()

field_html_renderer = FieldHtmlRenderer(log=log, api_2_field_as_map=api_2_field_as_map)

issue_page_html_renderer = IssuePageHtmlRenderer(log=log, export_path='export',
                                                 api_2_field_as_map=api_2_field_as_map,
                                                 filters=field_html_renderer.filters)
issue_page_html_renderer.copy_static_html_content_to_export_dir()

with open('resources/rest/KSP-1.json') as f:
    issue_json = json.load(f)

project_key, issue_key, issue_id = Tools.get_issue_info(issue_json)
issue_html = issue_page_html_renderer.render(issue_json)
issue_page_html_renderer.write(issue_html, project_key, issue_key, issue_id=issue_id)

with open('resources/rest/KSP-20.json') as f:
    issue_json = json.load(f)

project_key, issue_key, issue_id = Tools.get_issue_info(issue_json)
issue_html = issue_page_html_renderer.render(issue_json)
issue_page_html_renderer.write(issue_html, project_key, issue_key, issue_id=issue_id)
