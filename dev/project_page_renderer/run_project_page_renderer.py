import json
import logging

from field_html_renderer import FieldHtmlRenderer
from project_page_html_renderer import ProjectPageHtmlRenderer

api_2_field_as_map_dummy = {}
log = logging.getLogger()

field_html_renderer = FieldHtmlRenderer(log=log, api_2_field_as_map=api_2_field_as_map_dummy)

project_page_html_renderer = ProjectPageHtmlRenderer(log=log, export_path='export',
                                                     api_2_field_as_map=api_2_field_as_map_dummy,
                                                     filters=field_html_renderer.filters)

project_page_html_renderer.copy_static_html_content_to_export_dir()

with open('resources/KSP.json') as f:
    project_json = json.load(f)

project_html = project_page_html_renderer.render(project_json)
project_page_html_renderer.write(project_html, project_key='KSP')
