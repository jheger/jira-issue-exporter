import json
import sys
import time

from jira import JIRA

from issue_iterator import IssueIterator

jira = JIRA({
    "server": 'http://localhost:2990/jira'
}, basic_auth=('admin', 'admin'))


def create_issues(count):
    for i in range(1, count + 1):
        print(f"Creating issue {i}...")
        jira.create_issue(project='KSP', summary=f'New issue {i}',
                          description='Look into this one', issuetype={'name': 'Task'})


def query_issues(jql):
    # Note: we cast() for mypy's benefit, as search_issues can also return the raw json !
    # issues = cast(ResultList[Issue], jira.search_issues(jql, maxResults=1, fields='*all',
    #                          expand='renderedFields,changelog,names'))
    #
    #
    # issues = search_issues() has to problems:
    #   - The 'expand=names' is not in the return value 'issues'. The names are dismissed in the lib's
    #       function _fetch_pages().
    #   - The parameter issues.isLast is always False. This is because this parameter is read
    #       from Jira's response rather than calculate it. The isLast is not part of Jira's
    #       response of /api/2/search-search
    #

    is_last_page = False
    page_no = 0
    max_results = 50
    while not is_last_page:
        start_at = page_no * max_results
        issues = jira.search_issues(jql, startAt=start_at, maxResults=max_results, fields='*all',
                                    expand='renderedFields,changelog,names')
        # is_last_page = issues.isLast
        print(f"total: {issues.total}, page: {page_no}, start_at: {start_at}, isLast: {issues.isLast}")
        page_no += 1
        is_last_page = page_no * max_results >= issues.total
        # print(f"issue.fields.creator.displayName: {issue.fields.creator.displayName}")
        for issue in issues:
            print(f"issue: {issue}")
            # print(f"names: {issue.names}")
            # j = json.dumps(issue.raw)
            # print(f"issue raw: {issue.raw['names']}")


def query_issues_as_json_result(jql):
    # expand=names onyl functional in mode json_result=True !
    response = jira.search_issues(jql, startAt=0, maxResults=50, fields='*all',
                                expand='renderedFields,changelog,names'
                                , json_result=True
                                )
    # is_last_page = response.isLast
    print(f"response raw:  {response}")
    print(f"response JSON: {json.dumps(response)}")


def add_comments(issue_key, num):
    for n in range(0, num):
        jira.add_comment(issue_key, f"This is comment no C.{n}.")


def add_worklog(issue_key, timespent, num):
    for n in range(0, num):
        jira.add_worklog(issue_key, timespent)


def make_changelogs(issue_key, num):
    # "Text-field single line 1"
    issue = jira.issue(issue_key)
    for i in range(0, num):
        print(f"make_changelogs, #{i}")
        issue.update(fields={'customfield_10216': f'Text-field single line F.{i}'})
        #time.sleep(10)


def test_jira_issue_generator_pages():
    it = IssueIterator(jira_url='http://localhost:2990/jira/', jira_username='admin', jira_password='admin',
                           jql='project in (KSP)')

    for page in it.pages():
        print(f"it: {it}")
        #print(f"page: {page}")
        #print(f"page: no {it.page_no}, total {page['total']}")
        #sys.exit(1)
    print(f"it: {it}")


def test_jira_issue_generator_issues():
    it = IssueIterator(jira_url='http://localhost:2990/jira/', jira_username='admin', jira_password='admin',
                           jql='project in (KSP) ORDER BY KEY')

    for issue in it.issues():
        issue_key = issue['key']
        print(f"issue, issue_key: {issue_key}")


def eval_range():
    if 50 in range(0, 50):
        print("in range")
    else:
        print("not in range")


def eval_gen(num):
    if num == 0:
        print("num is 0, return.")
        return
    for n in range(num):
        yield n


def eval_gen_caller(num):
    for n in eval_gen(num):
        print(f"gen: {n}")

# query_issues('project=KSP')
#query_issues('key in (KSP-20,KSP-21)'2
#query_issues_as_json_result('key in (KSP-20,KSP-21)')
#add_comments('KSP-22', 1100)
#add_worklog('KSP-23', '2m', 50)
#make_changelogs('KSP-26', 1100)
#test_jira_issue_generator_pages()
test_jira_issue_generator_issues()
#eval_range()
#eval_gen_caller(0)