import json

from jira import JIRA

jira = JIRA({
    "server": 'http://localhost:2990/jira'
}, basic_auth=('admin', 'admin'))

j = jira.search_issues(jql_str='key in (KSP-29)', json_result=True, expand='renderedFields')
j_str = json.dumps(j, indent=4)
print(f"J: {j_str}")

