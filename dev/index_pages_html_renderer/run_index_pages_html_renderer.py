import json
# from bucketer import Bucketer
import logging

from field_html_renderer import FieldHtmlRenderer
from index_pages_html_renderer import IndexPagesHtmlRenderer
# fields_on_index_page:
#   Default-fields are:
#       issuekey,issuetype,summary,reporter,assignee,status,resolution
# Custom-fields must be given in the form customfield_12345.
from issue_page_html_renderer import IssuePageHtmlRenderer

config = {
    "fields_on_index_page": "issuekey,issuetype,summary,reporter,assignee,status,resolution"
}

with open('resources/rest/GET__api_2_field.json') as f:
    api_2_field = json.load(f)

api_2_field_as_map = {}
for field in api_2_field:
    api_2_field_as_map[field['id']] = field

log = logging.getLogger()

field_html_renderer = FieldHtmlRenderer(log=log, api_2_field_as_map=api_2_field_as_map)

issue_page_html_renderer = IssuePageHtmlRenderer(log=log, export_path='export',
                                                 api_2_field_as_map=api_2_field_as_map,
                                                 filters=field_html_renderer.filters)

issue_page_html_renderer.copy_static_html_content_to_export_dir()

index_pages_html_writer = IndexPagesHtmlRenderer(log=log, export_path='export',
                                                 api_2_field_as_map=api_2_field_as_map,
                                                 filters=field_html_renderer.filters,
                                                 export_datetime='Date/Time-Dummy')

index_pages_html_writer.total_issue_count = 12345

for i in range(1, 13):
    with open(f'resources/rest/KSP-{i}.json') as f:
        # renderd / html / projects / <project-key> / bucket
        # bucket = bucketer.calc_bucket('10000')
        # issue_path = str(pathlib.Path(bucketer.rendered_html_projects_path, 'KSP', bucket, f'KSP-{i}'))
        index_pages_html_writer.add_issue(json.load(f))
        if i % 5 == 0:
            index_pages_html_writer.render_and_write_subindex_page()

index_pages_html_writer.render_and_write_subindex_page()
index_pages_html_writer.render_and_write_topindex_page()
