import json
import re
from datetime import datetime

from jinja2 import Environment, FileSystemLoader

EXPORT_DIR = '../../export'
USE_DATESUFFUX_IN_EXPORT_DIR = False

g_fields_as_jsonobj = {}

jinja_environment = Environment(
    loader=FileSystemLoader('templates')
)


# Date format from REST response is "2021-08-17T15:35:53.942+0200".
# Make it less nit-picky to "2021-08-17 15:35:53 +0200".
# https://jinja.palletsprojects.com/en/3.0.x/api/#custom-filters
def render_datetime(value):
    if value:
        return re.sub('\.[0-9]+', ' ', value.replace('T', ' '))
    else:
        return ''


jinja_environment.filters['render_datetime'] = render_datetime


def render_date(value):
    return value


def render_group(value):
    if value and 'name' in value:
        return value['name']
    else:
        return value


def render_option(value):
    if value and 'value' in value:
        return value['value']
    else:
        return value


def render_option_with_child(value):
    val = ''
    if value and 'value' in value:
        val = f"{value['value']}"
        if 'child' in value and 'value' in value['child']:
            val += f" - {value['child']['value']}"
    return val


def render_project(value):
    if value and 'id' in value and 'name' in value and 'key' in value:
        return f"{value['name']} ({value['key']}, {value['id']})"
    else:
        return value


def render_user(value):
    if value and 'name' in value and 'key' in value \
            and 'displayName' in value and 'emailAddress' in value:
        return f"Name: {value['name']}, Key: {value['key']}" \
               f", Display-Name: {value['displayName']}, Email: {value['emailAddress']}"
    else:
        return value


def render_component_or_version(value):
    if value and 'id' in value and 'name' in value:
        return f"{value['name']} (Version-ID: {value['id']})"
    else:
        return value


def render_field_type(type, value):
    if not value:
        return value

    func_map = {
        'component': render_component_or_version,
        'date': render_date,
        'datetime': render_datetime,
        'group': render_group,
        'option': render_option,
        'option-with-child': render_option_with_child,
        'project': render_project,
        'user': render_user,
        'version': render_component_or_version
    }
    if not type in func_map:
        return str(value)

    f = func_map[type]
    if f:
        return f(value)
    else:
        return value


def render_field(issue, field_id: str):
    """

    Data-types:
        - array
        - "direct", one of the type listed in Item-types.

    Item-types:
        - date
        - datetime
        - group
        - number, but is already rendered by Jira.
        - option
        - option-with-child
        - project
        - string, but is already rendered by Jira.
        - user
        - version

    :param customfield_id: In the from customfield_12345.
    :return:
    """
    unrendered_field_value = issue['fields'][field_id]
    rendered_field_value = issue['renderedFields'][field_id]
    print(f"render_field(), field_id: {field_id}")

    if rendered_field_value:
        return rendered_field_value

    # It could be both the unrendered as well as the rendered value is null.
    if not unrendered_field_value:
        return unrendered_field_value

    # if not field_id.startswith('customfield_'):
    #    return unrendered_field_value

    # The field is absent in case it is hidden in the Field Configuration, and the
    # Field Configuration is the only one in the system.
    field_config = g_fields_as_jsonobj[field_id]
    if not field_config:
        return unrendered_field_value

    if 'items' not in field_config['schema']:
        # Is a "direct" type, not a list.
        return render_field_type(field_config['schema']['type'], unrendered_field_value)
    elif field_config['schema']['type'] == 'array':
        # If 'itemss' is given, it tells us the data-type, and 'type' tells us the collection type.
        rendered_values = []
        #print("AAAAAA {}".format(unrendered_field_value))
        for item in unrendered_field_value:
            rendered_values.append(render_field_type(field_config['schema']['items'], item))
            #print(f"  rendered_values: {rendered_values}")
        return ', '.join(rendered_values)
    else:
        return unrendered_field_value


jinja_environment.filters['render_field'] = render_field


def shall_render(field_id):
    if not field_id.startswith('customfield_'):
        return False
    field_config = g_fields_as_jsonobj[field_id]
    if not field_config:
        return False
    if field_config['schema']['type'] == 'any':
        # Don't know how to render this.
        return False
    return True


jinja_environment.tests['shall_render'] = shall_render


def now_to_date_string():
    return datetime.now().strftime('%Y-%m-%dT%H:%M:%S')


def create_export_dirname():
    if USE_DATESUFFUX_IN_EXPORT_DIR:
        return EXPORT_DIR + '_' + now_to_date_string()
    else:
        return EXPORT_DIR


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    # The fields-info seems to be independent from the configured renderers.
    fields_file = '../../examples/GET__rest_api_2_field__EN.json'
    with open(fields_file, 'r') as f1:
        fields_as_jsonobj = json.load(f1)
        for item in fields_as_jsonobj:
            g_fields_as_jsonobj[item['id']] = item
            print(f"FIELDS CONFIG: {item['id']}")

    infile = 'examples/GET__KSP-20__expand_changelog_names_renderedFields__admin_EN.json'
    with open(infile, 'r') as f2:
        infile_as_jsonobj = json.load(f2)
        issue_template = jinja_environment.get_template('issue.html.jinja')
        issue_html = issue_template.render(issue=infile_as_jsonobj)

        # print(f"HTML:\n{issue_html}")

        outfile = create_export_dirname() + '/issue.html'
        with open(outfile, 'w') as o:
            o.write(issue_html)
