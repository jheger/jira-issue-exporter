from bs4 import BeautifulSoup

rendered_text_field = """
<div>
    <!-- The renderedFields.description is given regardless of the Default Text Renderer
        or the Wiki Style Renderer, so it is save to use renderedFields.description
        rather than fields.description. But in case of the Default Text Renderer, it is
        not rendered of course. -->
    <p>This is the description.</p>

<p>Is has multiple lines.</p>

<p><font color="#FF0000"><ins><em><b>Some of them are formatted</b></em></ins></font>.</p>

<p>And there are two pictures in it:</p>

<p>This is picture 1:</p>

<p><span class="image-wrap" style=""><img src="/jira/secure/attachment/10300/10300_attachment-A.png" style="border: 0px solid black"></span></p>

<p>Ans this is picture 2:</p>

<p><span class="image-wrap" style=""><img src="/jira/secure/attachment/10301/10301_attachment-B.png" style="border: 0px solid black"></span></p>

<p>The pictures are from the same source as the attachments, but they're separate instances.</p>

<p>This is HTML markup as text: &lt;b&gt;bold&lt;/b&gt;.</p>
</div>
"""

soup = BeautifulSoup(rendered_text_field, "html.parser")
images = soup.find_all('img')
for image in images:
    url : str = image['src']
    print(f"URL: {url}")
    image['src'] = url.replace('/jira/secure/', '../my/local/dir/')

print(f"IMAGES: {images}")
print(f"SOUP: {soup}")
