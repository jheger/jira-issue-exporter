import json
import pathlib
import shutil
import unittest

from deepdiff import DeepDiff
from jira import JIRA

from downloader import Downloader
from jira_wrapper import JiraWrapper

config = {
    "jira_username": "admin",
    "jira_password": "admin",
    "jira_url": "http://localhost:2990/jira/",
    "export_path": "downloader/export",
    "append_datetime_to_export_path": False,
    "jql": "key in (KSP-20) ORDER BY key ASC",
    "export_attachments": True,
    "issue_count_per_index_page": 5
}

jira = JIRA({
    "server": config['jira_url']
}, basic_auth=(config['jira_username'], config['jira_password']))


class TestDownloader(unittest.TestCase):

    def test_01(self):
        """ Test if the JSON compares across Jira-versions.
        The language of 'admin' has to be to EN. """
        self.total = -1
        self.on_issue_callback_count = 0

        # ignore_errors=True: If the directory is not present, rmtree() treats this as an error.
        # Ignore this.
        shutil.rmtree(config['export_path'], ignore_errors=True)

        def total_callback(total_issue_count):
            self.total = total_issue_count

        def on_issue_callback(issue):
            self.on_issue_callback_count += 1

        jira_wrapper = JiraWrapper(jira=jira)

        downloader = Downloader(config['export_path'], jira_wrapper, config['jql'],
                                total_callback=total_callback, on_issue_callback=on_issue_callback)

        downloader.download()

        with open(pathlib.Path('downloader', 'ref', 'api_2_field.json'), 'r') as f:
            ref_api_2_field = json.loads(f.read())

        with open(pathlib.Path(config['export_path'], 'downloaded', 'misc', 'api_2_field.json'), 'r') as f:
            got_api_2_field = json.loads(f.read())

        # The fields 'archiveddate' and 'archivedby' are newer than the ealiest tested version Jira 8.0.0.
        # Remove them.
        # I tried with DeepDiff's exclude_regex_paths as well as exclude_obj_callback but couldn't get
        # them ignoring those fields.
        got_api_2_field = [i for i in got_api_2_field if i['id'] not in ['archiveddate', 'archivedby']]

        ddiff = DeepDiff(ref_api_2_field, got_api_2_field, ignore_order=True)
        self.assertFalse(ddiff)
        self.assertEqual(1, self.total)
        self.assertEqual(1, self.on_issue_callback_count)

        with open(pathlib.Path('downloader', 'ref', 'KSP.json'), 'r') as f:
            ref_ksp = json.loads(f.read())

        with open(pathlib.Path(config['export_path'], 'downloaded', 'issues', 'KSP', 'KSP.json'), 'r') as f:
            got_ksp = json.loads(f.read())

        ddiff = DeepDiff(ref_ksp, got_ksp)
        self.assertFalse(ddiff)

        with open(pathlib.Path('downloader', 'ref', 'KSP-20.json'), 'r') as f:
            ref_ksp_20 = json.loads(f.read())

        with open(pathlib.Path(config['export_path'], 'downloaded', 'issues', 'KSP', '10000', 'KSP-20.json'), 'r') as f:
            got_ksp_20 = json.loads(f.read())

        #
        # About the excluded path:
        #
        # renderedFields: Contains relative dates like "5 minutes ago" rather than comparable absolute dates.
        # lastViewed: Depends on the last view and could change over time.
        # customfield_10100: Field "Development". Is serializes a bead-address which is different at each export.
        # customfield_10205: Field "Development Summary (CF)". Is serializes a bead-address which is different at
        # each export. This field has been removed from the screen but is still in the issue.
        #
        # In Jira version TODO, the attribte 'disabled' was added to some field-types. The following fields
        # are affected.
        #
        # customfield_10200:
        #   Checkboxes
        #   com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes
        # customfield_10201:
        #    "Checkboxes with 3 checkboxes (CF)"
        #   com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes
        # customfield_10211:
        #   Radio Button
        #   com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons
        # customfield_10212:
        #   Select List (cascading)
        #   com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect
        # customfield_10213:
        #   Select List (multiple choices)
        #   com.atlassian.jira.plugin.system.customfieldtypes:multiselect
        # customfield_10214:
        #   Select List (single choice)
        #   com.atlassian.jira.plugin.system.customfieldtypes:select
        #
        exclude_regex_paths = [r"root\['renderedFields'\]",
                               r"root\['fields'\]\['lastViewed'\]",
                               r"root\['fields'\]\['customfield_10100'\]",
                               r"root\['fields'\]\['customfield_10200'\]\[\d+\]\['disabled'\]",
                               r"root\['fields'\]\['customfield_10201'\]\[\d+\]\['disabled'\]",
                               r"root\['fields'\]\['customfield_10205'\]",
                               r"root\['fields'\]\['customfield_10211'\]\['disabled'\]",
                               r"root\['fields'\]\['customfield_10212'\]\['disabled'\]",
                               r"root\['fields'\]\['customfield_10212'\]\['child'\]\['disabled'\]",
                               r"root\['fields'\]\['customfield_10213'\]\[\d+\]\['disabled'\]",
                               r"root\['fields'\]\['customfield_10214'\]\['disabled'\]",
                               r"root\['fields'\]\['archiveddate'\]",
                               r"root\['fields'\]\['archivedby'\]"]
        ddiff = DeepDiff(ref_ksp_20, got_ksp_20, exclude_regex_paths=exclude_regex_paths)
        self.assertFalse(ddiff)
