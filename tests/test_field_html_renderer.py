import json
import logging
import unittest

from field_html_renderer import FieldHtmlRenderer


class TestTools(unittest.TestCase):
    log = logging.getLogger()

    api_2_field_as_map = {}

    def setUp(self) -> None:
        with open('resources/jira-8.19.0/GET__api_2_field.json') as f:
            api_2_field = json.load(f)
            for field in api_2_field:
                self.api_2_field_as_map[field['id']] = field

        self.field_html_renderer = FieldHtmlRenderer(log=self.log, api_2_field_as_map=self.api_2_field_as_map)

    def test_render_sprint(self):
        field_value = None
        rendered_field = self.field_html_renderer.render_sprint(field_value)
        self.assertEqual('', rendered_field)

        field_value = ''
        rendered_field = self.field_html_renderer.render_sprint(field_value)
        self.assertEqual('', rendered_field)

        # A sprint field value seen in Jira 8.0.0, one single sprint.
        field_value = 'com.atlassian.greenhopper.service.sprint.Sprint@192fa628[id=1,rapidViewId=2' \
                      ',state=ACTIVE,name=Sample Sprint 1,startDate=2021-08-09T04:57:46.520+02:00' \
                      ',endDate=2021-08-23T05:17:46.520+02:00,completeDate=<null>,sequence=1,goal=<null>]'
        rendered_field = self.field_html_renderer.render_sprint(field_value)
        self.assertEqual('Sample Sprint 1 [1]', rendered_field)

    def test_render_field_for_srint_field_with_no_sprint(self):
        issue = {
            "id": "10109",
            "key": "SEP-10",
            "fields": {
                "customfield_10001": None
            },
            "renderedFields": {
                "customfield_10001": None
            }
        }

        field_id = 'customfield_10001'
        rendered_field = self.field_html_renderer.render_field(issue=issue, field_id=field_id)
        self.assertEqual('', rendered_field)

    def test_render_field_for_srint_field_with_1_sprint(self):
        issue = {
            "id": "10109",
            "key": "SEP-10",
            "fields": {
                "customfield_10001": [
                    "com.atlassian.greenhopper.service.sprint.Sprint@1fc5abea[id=1,rapidViewId=2,"
                    "state=ACTIVE,name=Sample Sprint 2,startDate=2021-08-09T04:57:46.520+02:00,"
                    "endDate=2021-08-23T05:17:46.520+02:00,completeDate=<null>,"
                    "activatedDate=2021-08-09T04:57:46.520+02:00,sequence=1,goal=<null>,autoStartStop=false]"
                ]
            },
            "renderedFields": {
                "customfield_10001": None
            }
        }

        field_id = 'customfield_10001'
        rendered_field = self.field_html_renderer.render_field(issue=issue, field_id=field_id)
        self.assertEqual('Sample Sprint 2 [1]', rendered_field)

    def test_render_field_for_srint_field_with_1_sprint_with_special_chars_in_name(self):
        issue = {
            "id": "10109",
            "key": "SEP-10",
            "fields": {
                "customfield_10001": [
                    "com.atlassian.greenhopper.service.sprint.Sprint@1fc5abea[id=1,rapidViewId=2,"
                    "state=ACTIVE,name=Sample,+= Sprint 2,startDate=2021-08-09T04:57:46.520+02:00,"
                    "endDate=2021-08-23T05:17:46.520+02:00,completeDate=<null>,"
                    "activatedDate=2021-08-09T04:57:46.520+02:00,sequence=1,goal=<null>,autoStartStop=false]"
                ]
            },
            "renderedFields": {
                "customfield_10001": None
            }
        }

        field_id = 'customfield_10001'
        rendered_field = self.field_html_renderer.render_field(issue=issue, field_id=field_id)
        self.assertEqual('Sample,+= Sprint 2 [1]', rendered_field)

    def test_render_field_for_srint_field_with_2_sprints(self):
        issue = {
            "id": "10109",
            "key": "SEP-10",
            "fields": {
                "customfield_10001": [
                    "com.atlassian.greenhopper.service.sprint.Sprint@7b166399"
                    "[id=1,rapidViewId=2,state=CLOSED,name=Sample Sprint 1"
                    ",startDate=2021-08-09T04:57:46.520+02:00,endDate=2021-08-23T05:17:46.520+02:00"
                    ",completeDate=2021-09-12T19:35:35.726+02:00,activatedDate=2021-08-09T04:57:46.520+02:00"
                    ",sequence=1,goal=<null>,autoStartStop=false]",
                    "com.atlassian.greenhopper.service.sprint.Sprint@14c35f65"
                    "[id=4,rapidViewId=3,state=ACTIVE,name=Sample Sprint 2"
                    ",startDate=2021-09-05T10:22:19.217+02:00,endDate=2021-09-19T10:42:19.217+02:00"
                    ",completeDate=<null>,activatedDate=2021-09-05T10:22:19.217+02:00"
                    ",sequence=4,goal=<null>,autoStartStop=false]"
                ],
            },
            "renderedFields": {
                "customfield_10001": None
            }
        }

        field_id = 'customfield_10001'
        rendered_field = self.field_html_renderer.render_field(issue=issue, field_id=field_id)
        self.assertEqual('Sample Sprint 1 [1], Sample Sprint 2 [4]', rendered_field)
