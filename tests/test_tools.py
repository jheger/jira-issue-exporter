import unittest

from tools import Tools


class TestTools(unittest.TestCase):

    def test_calc_bucket(self):
        self.assertEqual(Tools.calc_bucket('AA-1'), '10000')
        self.assertEqual(Tools.calc_bucket('AA-9999'), '10000')
        self.assertEqual(Tools.calc_bucket('AA-10000'), '10000')
        self.assertEqual(Tools.calc_bucket('AA-10001'), '20000')
        self.assertEqual(Tools.calc_bucket('AA-19999'), '20000')
        self.assertEqual(Tools.calc_bucket('AA-20000'), '20000')
        self.assertEqual(Tools.calc_bucket('AA-20001'), '30000')
        self.assertEqual(Tools.calc_bucket('AA-29999'), '30000')

    def test_add_order_by_clause_if_absend(self):
        jql = 'key in (AA-123)'
        self.assertEqual(Tools.add_order_by_clause_if_absend(jql), jql + ' ORDER BY key')

        jql = 'key in (AA-123) ORDER BY type'
        self.assertEqual(Tools.add_order_by_clause_if_absend(jql), jql)

        jql = 'key in (AA-123) order by type'
        self.assertEqual(Tools.add_order_by_clause_if_absend(jql), jql)
