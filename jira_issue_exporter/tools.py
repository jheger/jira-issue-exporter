import json
from dataclasses import dataclass
from math import ceil


@dataclass
class Tools:

    @staticmethod
    def calc_bucket(issue_key: str):
        """ Divide issues and attachments into "buckets" of 10000. This is to not exceed the filesystem's
        limit of max. files per directory.
        The bucket is calculated by the issue-number. E.g. "1234" of issue-key ABC-1234.

        From "How is the attachment storage path constructed in JIRA?":
            (https://community.atlassian.com/t5/Jira-Core-Server-questions/How-is-the-attachment-storage-path-constructed-in-JIRA/qaq-p/6312)
            > Operating systems and their filesystems impose certain limits on the number of files that can be
            > contained in a single directory. On ext3, for example, you may not have more than 65,535 entries in a
            > single directory.  If we used just the issue key, then as your product grew:
            > At some point you wouldn't be able to attach files to any new issues because the directory entry slots
            would be exhausted.
        More refs:
            - https://confluence.atlassian.com/jirakb/locate-jira-server-file-attachments-in-the-filesystem-859487788.html
            - https://docs.atlassian.com/software/jira/docs/api/7.4.1/com/atlassian/jira/issue/attachment/FileAttachments.html

        :param issue_key: The issue-key
        :return: Bucket (10000, 20000,...)
        """
        issue_num = issue_key.split('-')[1]
        bucket_size = 10000.0
        bucket = ceil(int(issue_num) / bucket_size) * bucket_size
        # bucket is a float in the form 10000.0. Make it a string and strip off the .0.
        return str(bucket).replace('.0', '')

    @staticmethod
    def get_issue_info(issue_json):
        project_key = issue_json['key'].split('-')[0]
        issue_key = issue_json['key']
        issue_id = issue_json['id']
        return project_key, issue_key, issue_id

    @staticmethod
    def build_local_attachment_name(attachment_id, attachment_filename):
        return f'{attachment_id}_{attachment_filename.replace(" ", "+")}'

    @staticmethod
    def get_attachment_thumb_name(thumbnail_url):
        # Get last part of the URL.
        return thumbnail_url.split('/')[-1]

    @staticmethod
    def add_order_by_clause_if_absend(jql: str):
        if jql.lower().find('order by ') == -1:
            return jql + ' ORDER BY key'
        else:
            return jql

    @staticmethod
    def write_textfile_with_encoding(path, text=None, json_obj=None):
        # It is pretty sure Jira's response comes in UTF-8, so write back in this encoding.
        with open(path, 'w', encoding='utf-8') as f:
            s = text if text else json.dumps(json_obj, indent=4)
            f.write(s)
