from dataclasses import dataclass, field
from logging import Logger
from typing import Any, List

from jira import JIRA
from jira.client import ResultList


@dataclass
class CallbackFunc:
    i: int
    func: Any


@dataclass
class IssueIterator:
    log: Logger
    jira: JIRA
    jql: str
    page_size: int = field(default=50)
    total: int = field(default=0, init=False)
    start_at: int = field(default=0, init=False)
    page_no: int = field(default=0, init=False)
    is_last_page: bool = field(default=False, init=False)
    response: dict = field(init=False, repr=False)
    total_callback: Any = field(init=True, default=None, repr=False)
    has_total_callback_been_called: bool = field(init=False, default=False)
    callback_function_registry: List[CallbackFunc] = field(default_factory=list, init=False)
    result: ResultList = None

    def register_on_nth_and_after_last(self, i, callback_func):
        self.callback_function_registry.append(CallbackFunc(i=i, func=callback_func))

    def pages(self):
        """ Generator-function to return paged search-results page by page. """

        self.log.debug(f"jql {self.jql}, page_size {self.page_size}")

        while not self.is_last_page:
            # self.page_size is set to 50. This avoids reading the maxResults from the response.
            self.start_at = self.page_no * self.page_size
            self.log.debug(f"start_at {self.start_at}")
            # Info about json_result=True:
            # In previous dev-versions of this class the 'expand=names' was used. But 'names' is not in the return
            # value 'issues' when calling with the default json_result=False.
            # The names are dismissed in the lib's internal function _fetch_pages()!
            # So I had to use the json_result=True.
            # In the meantime I get the field-names from /rest/api/2/field.
            # I haven't rewritten the code to use json_result=False (the default).
            # TODO rewrite to json_result=False
            #
            # About the response:
            #   Contains startAt, maxResults, total, issues.
            self.response = self.jira.search_issues(self.jql, startAt=self.start_at, maxResults=self.page_size,
                                                    fields='*all', expand='renderedFields,changelog', json_result=True)
            self.total = self.response['total']
            if not self.has_total_callback_been_called and self.total_callback:
                self.log.debug(f"Calling total_callback function. total: {self.total}")
                self.total_callback(self.total)
                self.has_total_callback_been_called = True

            # Check:
            # self.page_no * self.page_size >= self.total >>>>> is_last_page
            #       1      *    50          >=      0               True
            #       1      *    50          >=      1               True
            #       1      *    50          >=      49              True
            #       1      *    50          >=      50              True
            #       1      *    50          >=      51              False
            #       2      *    50          >=      51              True
            #   ...
            self.is_last_page = (self.page_no + 1) * self.page_size >= self.total
            self.log.debug(f"Response: startAt {self.response['startAt']}"
                           f", maxResults {self.response['maxResults']}"
                           f", total {self.total}, is_last_page {self.is_last_page}")

            yield self.response

            # This is to not confuse someone printing the __repr__() after the past page has been read.
            if not self.is_last_page:
                self.page_no += 1
                self.start_at += self.page_size

    def issues(self):
        i = 0
        for page in self.pages():
            for issue in page['issues']:
                yield issue
                i += 1
                # After nth item:
                for callback_item in self.callback_function_registry:
                    idx = callback_item.i
                    func = callback_item.func
                    if i % idx == 0:
                        func(issue, i - 1)
        # After last item:
        for callback_item in self.callback_function_registry:
            callback_item.func(False, i - 1)

    def __iter__(self):
        return self.issues()
