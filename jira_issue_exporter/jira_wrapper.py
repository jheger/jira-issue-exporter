from dataclasses import dataclass
from logging import Logger

from jira import JIRA, Project


@dataclass
class JiraWrapper:
    log: Logger
    jira: JIRA

    def get_api_2_field(self):
        f = self.jira._session.get(url=self.jira._get_url('field'))
        return f.json()

    def get(self, full_url, **kwargs):
        r = self.jira._session.get(url=full_url, **kwargs)
        return r.content

    def get_project(self, project_key):
        """ Get the project-JSON. The built-in function jira.project(project_key) doesn't take the expand-parameter.
        But to get the attribute projectKeys the parameter expand=projectKey is needed. Fortunately the internal
        function _find_for_resource() takes the expand-parameter. """
        project = self.jira._find_for_resource(Project, project_key, expand='projectKeys')
        return project

    def wiki_renderer(self, full_url, unrendered_markup):
        """ Seen in https://jira.atlassian.com/browse/JRASERVER-38731. """
        if not unrendered_markup:
            return unrendered_markup

        # One more attribute is 'issueKey': '' seen in JRASERVER-38731. But it seems the API is also happy without this.
        # TODO: escape it ?
        data = {
            'rendererType': 'atlassian-wiki-renderer',
            'unrenderedMarkup': unrendered_markup
        }
        r = self.jira._session.post(url=full_url, data=data)
        return r
