import pathlib
from dataclasses import dataclass
from typing import Any

from base_html_renderer import BaseHtmlRenderer
from tools import Tools


@dataclass
class IssuePageHtmlRenderer(BaseHtmlRenderer):
    local_rel_attachment_base_url: str = '../../../../../downloaded/attachments'

    def __post_init__(self):
        super().__post_init__()

        self.rendered_html_projects_path = pathlib.Path(self.rendered_html_path, 'projects')

    def render(self, issue):
        """ Render an issue, remove comments to reduce page-size, normalize line-endings not to confuse editors. """

        issue_template = self.jinja_environment.get_template('issue_page.html.jinja')
        issue_html = issue_template.render(api_2_field_as_map=self.api_2_field_as_map, issue=issue)
        issue_html = self.remove_comments_and_beautify(issue_html)
        return issue_html

    def write(self, html, project_key, issue_key, bucket=None, issue_id=None):
        if not bucket and not issue_id:
            raise ValueError(f"Need either bucket or issue_id. Got: bucket {bucket}, issue_id {issue_id}")
        p = pathlib.Path(
            self.rendered_html_projects_path,
            project_key,
            bucket if bucket else Tools.calc_bucket(issue_key))
        p.mkdir(parents=True, exist_ok=True)
        Tools.write_textfile_with_encoding(pathlib.Path(p, issue_key + '.html'), text=html)
