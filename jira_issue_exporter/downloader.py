import json
import pathlib
from dataclasses import dataclass, field
from logging import Logger
from multiprocessing.pool import ThreadPool
from typing import Any

from issue_iterator import IssueIterator
from jira_wrapper import JiraWrapper
from tools import Tools


@dataclass
class Downloader:
    log: Logger
    export_dir: str
    jira_wrapper: JiraWrapper
    issue_iterator: IssueIterator
    page_size: int = 50
    on_issue_callback: Any = field(default=None, repr=False)
    on_project_callback: Any = field(default=None, repr=False)
    api_2_field_as_map: dict = field(init=False)
    project_keys: set = field(default_factory=set, init=False)
    attachments: list = field(default_factory=list, init=False)

    def download(self):

        self.api_2_field_as_map = self.download_api_2_field()

        self.issue_iterator.register_on_nth_and_after_last(50, self._on_nth_and_last_issue)

        for issue in self.issue_iterator:
            project_key, issue_key, issue_id = Tools.get_issue_info(issue)
            self.project_keys.add(project_key)
            bucket = Tools.calc_bucket(issue_key)

            bucket_path = pathlib.Path(self.export_dir, 'downloaded', 'issues', project_key, bucket)
            bucket_path.mkdir(parents=True, exist_ok=True)
            Tools.write_textfile_with_encoding(pathlib.Path(bucket_path, issue_key + '.json'), json_obj=issue)

            for attachment in issue['fields']['attachment']:
                attachment_path = pathlib.Path(self.export_dir, 'downloaded', 'attachments', project_key, bucket,
                                               issue_key)

                url = attachment['content']
                path = attachment_path
                name = Tools.build_local_attachment_name(attachment['id'], attachment['filename'])
                self.attachments.append((url, path, name))

                # Only images have a thumbnail, but documents haven't.
                if 'thumbnail' in attachment:
                    thumbnail_url = attachment['thumbnail']
                    url = attachment['thumbnail']
                    path = pathlib.Path(attachment_path, 'thumbs')
                    name = Tools.get_attachment_thumb_name(thumbnail_url)
                    self.attachments.append((url, path, name))

            if self.on_issue_callback:
                self.on_issue_callback(issue)

        for project_key in self.project_keys:
            project = self.jira_wrapper.get_project(project_key)
            project_json_path = pathlib.Path(self.export_dir, 'downloaded', 'issues', project_key,
                                             project_key + '.json')
            Tools.write_textfile_with_encoding(project_json_path, json_obj=project.raw)

            if self.on_project_callback:
                self.on_project_callback(project.raw)

    def download_api_2_field(self):
        api_2_field = self.jira_wrapper.get_api_2_field()
        misc_path = pathlib.Path(self.export_dir, 'downloaded', 'misc', )
        misc_path.mkdir(parents=True, exist_ok=True)
        with open(pathlib.Path(misc_path, 'api_2_field.json'), 'w') as f:
            f.write(json.dumps(api_2_field, indent=4))

        api_2_field_as_map = {}
        for field in api_2_field:
            api_2_field_as_map[field['id']] = field

        return api_2_field_as_map

    def _on_nth_and_last_issue(self, issue, i):
        self.log.debug(f"Submitting {len(self.attachments)} attachment-URLs for downloading...")
        # self.log.debug(f"  attachments: {self.attachments}")
        with ThreadPool(5) as tp:
            tp.imap_unordered(self._download_attachment, self.attachments)
            tp.close()
            tp.join()
            self.log.debug(f"Downloading {len(self.attachments)} attachments done.")
        self.attachments = []

    def _download_attachment(self, entry):
        self.log.debug(f"entry {entry}")
        url, path, name = entry
        bin = self.jira_wrapper.get(url)
        path.mkdir(parents=True, exist_ok=True)
        with open(pathlib.Path(path, name), 'wb') as f:
            f.write(bin)
