import json
import logging
import pathlib
import sys
from datetime import datetime

import urllib3
from jira import JIRA

from downloader import Downloader
from field_html_renderer import FieldHtmlRenderer
from index_pages_html_renderer import IndexPagesHtmlRenderer
from issue_iterator import IssueIterator
from issue_page_html_renderer import IssuePageHtmlRenderer
from jira_wrapper import JiraWrapper
from project_page_html_renderer import ProjectPageHtmlRenderer
from tools import Tools

VALID_LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
DEFAULT_LOG_LEVEL = 'INFO'
DEFAULT_CONFIG = {
    'export_path': './export',
    'append_datetime_to_export_path': False,
    'issue_count_per_index_page': 1000,
    'loglevel': DEFAULT_LOG_LEVEL,
    'page_size': 50
}


def configure_logging(loglevel):
    log = logging.getLogger()
    # Set basicConfig() to get levels less than WARNING running in our logger.
    # See https://stackoverflow.com/questions/56799138/python-logger-not-printing-info
    logging.basicConfig(level='INFO')
    # Set a useful logging-format. Not the most elegant way, but it works.
    log.handlers[0].setFormatter(
        logging.Formatter('%(asctime)s:%(levelname)s:%(module)s:%(funcName)s %(message)s'))
    # See also https://docs.python.org/3/howto/logging.html:
    numeric_level = getattr(logging, loglevel.upper(), None)
    # The check for valid values have been done in validate_config().
    log.setLevel(numeric_level)
    # TODO ?? Adjust logging-level of module "urllib3". If our logging is set to DEBUG, that also logs in that level.
    # logging.getLogger('urllib3').setLevel(logging.WARNING)

    return log


def validate_config(config):
    errors = []
    mandatory_parameters = ['jira_username', 'jira_password', 'jira_url', 'jql']

    for param in mandatory_parameters:
        if param not in config:
            errors.append(f"Mandatory parameter '{param}' missing in config-file.")
        elif not config[param]:
            errors.append(f"Mandatory parameter '{param}' is empty.")

    if str(config['loglevel']).upper() not in VALID_LOG_LEVELS:
        errors.append(f"Loglevel \'{config['loglevel']}\' is invalid. Expect one of {', '.join(VALID_LOG_LEVELS)}")

    try:
        page_size = config['page_size']
        int(page_size)
        if page_size < 50:
            raise ValueError
    except ValueError:
        errors.append(f"page_size \'{config['page_size']}\' invalid. Expect an positive integer >= 50.")

    try:
        page_size = config['issue_count_per_index_page']
        int(page_size)
        if page_size < IndexPagesHtmlRenderer.MIN_ISSUES_PER_SUBINDEX_PAGE:
            raise ValueError
    except ValueError:
        errors.append(
            f"issue_count_per_index_page \'{config['issue_count_per_index_page']}\' invalid."
            f"Expect an positive integer >= {IndexPagesHtmlRenderer.MIN_ISSUES_PER_SUBINDEX_PAGE}.")

    return errors


def datetime_to_date_string(dt):
    return dt.strftime('%Y-%m-%dT%H:%M:%S')


def main():
    start_timestamp = datetime.now()
    start_timestamp_as_str = datetime_to_date_string(start_timestamp)

    if len(sys.argv) != 2:
        raise ValueError("Need 1 command line argument for the config-file.")

    log = configure_logging('INFO')

    config_file = sys.argv[1]
    with open(config_file) as f:
        config = json.load(f)
        effective_config = DEFAULT_CONFIG.copy()
        effective_config.update(config)
        effective_config['jql'] = Tools.add_order_by_clause_if_absend(effective_config['jql'])
        sanitized_effective_config = effective_config.copy()
        sanitized_effective_config['jira_password'] = '<sanitized>'
        log.info(f"Effective config: {sanitized_effective_config}")
        config_errors = validate_config(effective_config)
        if config_errors:
            raise ValueError(f"Config-errors: {config_errors}")

    numeric_level = getattr(logging, effective_config['loglevel'].upper(), None)
    log.setLevel(numeric_level)

    log.info("Started")

    export_pathname = effective_config['export_path']
    if effective_config['append_datetime_to_export_path']:
        export_pathname += start_timestamp_as_str

    # Check for existing download directory. Stop if some exists to not overwrite a previous download.
    if pathlib.Path(export_pathname).is_dir():
        raise ValueError(f"export_path '{export_pathname}' already exists. Abort.")

    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    jira = JIRA({
        'server': effective_config['jira_url'],
        'verify': False,
    }, basic_auth=(effective_config['jira_username'], effective_config['jira_password']))

    jira_wrapper = JiraWrapper(log=log, jira=jira)

    def total_callback(total):
        index_pages_html_writer.total_issue_count = total

    def on_issue_callback(issue_json):
        issue_html = issue_page_html_renderer.render(issue_json)
        project_key, issue_key, issue_id = Tools.get_issue_info(issue_json)
        issue_page_html_renderer.write(issue_html, project_key, issue_key, issue_id=issue_id)
        index_pages_html_writer.add_issue(issue_json)

    def on_nth_and_last_issue(issue, i):
        index_pages_html_writer.render_and_write_subindex_page()

    def on_project_callback(project_json):
        project_html = project_page_html_renderer.render(project_json)
        project_page_html_renderer.write(project_html, project_key=project_json['key'])

    issue_iterator = IssueIterator(log=log, jira=jira_wrapper.jira, jql=effective_config['jql'],
                                   total_callback=total_callback, page_size=effective_config['page_size'])

    downloader = Downloader(log=log, issue_iterator=issue_iterator, export_dir=export_pathname,
                            jira_wrapper=jira_wrapper,
                            on_issue_callback=on_issue_callback,
                            on_project_callback=on_project_callback)

    issue_iterator.register_on_nth_and_after_last(effective_config['issue_count_per_index_page'], on_nth_and_last_issue)

    api_2_field_as_map = downloader.download_api_2_field()

    field_html_renderer = FieldHtmlRenderer(log=log, api_2_field_as_map=api_2_field_as_map)

    issue_page_html_renderer = IssuePageHtmlRenderer(log=log, export_path=export_pathname,
                                                     api_2_field_as_map=api_2_field_as_map,
                                                     filters=field_html_renderer.filters)

    issue_page_html_renderer.copy_static_html_content_to_export_dir()

    project_page_html_renderer = ProjectPageHtmlRenderer(log=log, export_path=export_pathname,
                                                         api_2_field_as_map=api_2_field_as_map,
                                                         filters=field_html_renderer.filters)

    index_pages_html_writer = IndexPagesHtmlRenderer(log=log, export_path=export_pathname,
                                                     api_2_field_as_map=api_2_field_as_map,
                                                     export_datetime=start_timestamp_as_str.replace('_', ' '),
                                                     filters=field_html_renderer.filters)

    downloader.download()
    index_pages_html_writer.render_and_write_topindex_page()

    finish_timestamp = datetime.now()
    log.info(f"Finished. Took {finish_timestamp - start_timestamp}")


if __name__ == '__main__':
    main()
