import re
from dataclasses import dataclass
from logging import Logger

from bs4 import BeautifulSoup

from tools import Tools


@dataclass
class FieldHtmlRenderer:
    log: Logger
    api_2_field_as_map: dict
    local_rel_attachment_base_url: str = '../../../../../downloaded/attachments'

    cls_api_2_field_as_map = {}

    def __post_init__(self):
        FieldHtmlRenderer.cls_api_2_field_as_map = self.api_2_field_as_map

    @staticmethod
    def render_component_or_version_type(value):
        if value and 'id' in value and 'name' in value:
            return f"{value['name']} (Version-ID: {value['id']})"
        else:
            return value

    # Date format from REST response is "2021-08-17T15:35:53.942+0200".
    # Make it less nit-picky to "2021-08-17 15:35:53 +0200".
    @staticmethod
    def render_datetime_type(value):
        if value:
            # 1. Remove the millis, and 2. remoce the delimiter 'T'.
            return re.sub(r'\.[0-9]+', ' ', value.replace('T', ' '))
        else:
            return ''

    @staticmethod
    def render_option_with_child_type(value):
        val = ''
        if value and 'value' in value:
            val = f"{value['value']}"
            if 'child' in value and 'value' in value['child']:
                val += f" - {value['child']['value']}"
        return val

    @staticmethod
    def render_project_type(value):
        if value and 'id' in value and 'name' in value and 'key' in value:
            return f"{value['name']} ({value['key']}, Project-ID: {value['id']})"
        else:
            return value

    @staticmethod
    def render_user_type(value):
        if value and 'name' in value and 'key' in value \
                and 'displayName' in value and 'emailAddress' in value:
            return f"Name: {value['name']}, Key: {value['key']}" \
                   f", Display-Name: {value['displayName']}, Email: {value['emailAddress']}"
        else:
            return value

    @staticmethod
    def render_votes_type(value):
        if value and 'votes' in value:
            return value['votes']
        else:
            return value

    @staticmethod
    def render_watches_type(value):
        if value and 'watchCount' in value:
            return value['watchCount']
        else:
            return value

    @staticmethod
    def render_name_attr(value):
        if value and 'name' in value:
            return value['name']
        else:
            return value

    @staticmethod
    def render_value_attr(value):
        if value and 'value' in value:
            return value['value']
        else:
            return value

    @staticmethod
    def render_through(value):
        return value

    # noinspection PyUnusedLocal
    @staticmethod
    def render_none(value):
        return None

    @staticmethod
    def render_sprint(value):
        if not value:
            return ''
        # The value is something like in the following examples. The order of the attributes seems to be
        # constant across Jira-verions (but the amount of attributes has increased).
        #
        # Jira 8.0.0:
        #   com.atlassian.greenhopper.service.sprint.Sprint@192fa628[id=1,rapidViewId=2,state=ACTIVE,\
        #   name=Sample Sprint 2,startDate=2021-08-09T04:57:46.520+02:00,endDate=2021-08-23T05:17:46.520+02:00,\
        #   completeDate=<null>,sequence=1,goal=<null>]
        #
        # Jira 8.10.0:
        #   com.atlassian.greenhopper.service.sprint.Sprint@3b5e55e8[id=1,rapidViewId=2,state=ACTIVE,\
        #   name=Sample Sprint 2,startDate=2021-08-09T04:57:46.520+02:00,endDate=2021-08-23T05:17:46.520+02:00,\
        #   completeDate=<null>,activatedDate=2021-08-09T04:57:46.520+02:00,sequence=1,goal=<null>,autoStartStop=false]
        name = re.sub(r'.+\bname=(.+),startDate=.+', r'\1', value)
        id = re.sub(r'.+\bid=(.+),rapidViewId=.+', r'\1', value)
        # print(f"NAME: {name}")
        # print(f"ID: {id}")
        return f"{name} [{id}]"

    @classmethod
    def render_field_type(cls, field_type, value, custom=None):
        if not value:
            return value

        # The following dict maps the data-types to render-functions.
        # Some data-types look similar to field-ids, but there is a differnce between them.
        # E.g. look at the data for Components (from GET api_2_field):
        # The field-id is "components", and the data-type in schema.type is "an array of items of type component".
        #
        #       {
        #         "id": "components",
        #         "name": "Component/s",
        #         "custom": false,
        #         "orderable": true,
        #         "navigable": true,
        #         "searchable": true,
        #         "clauseNames": [
        #             "component"
        #         ],
        #         "schema": {
        #             "type": "array",          # <-- is an array...
        #             "items": "component",     # ... then this is the type.
        #             "system": "components"
        #         }
        #     }
        #
        # Not in any case the schema.items is a proper type-hint:
        # The field "sprint" has items of type "string", but in fact this string represents an object.
        #
        #     {
        #         "id": "customfield_10001",
        #         "name": "Sprint",
        #         "custom": true,
        #         "orderable": true,
        #         "navigable": true,
        #         "searchable": true,
        #         "clauseNames": [
        #             "cf[10001]",
        #             "Sprint"
        #         ],
        #         "schema": {
        #             "type": "array",
        #             "items": "string",
        #             "custom": "com.pyxis.greenhopper.jira:gh-sprint",
        #             "customId": 10001
        #         }
        #     }
        #
        # A sprint-field's value in issue.fields could be:
        #
        #   com.atlassian.greenhopper.service.sprint.Sprint@192fa628[id=1,rapidViewId=2,state=ACTIVE,\
        #   name=Sample Sprint 2,startDate=2021-08-09T04:57:46.520+02:00,endDate=2021-08-23T05:17:46.520+02:00,\
        #   completeDate=<null>,sequence=1,goal=<null>]
        #
        # Technically this is a string, but it is nothing we should output to the user.
        # Therefore the function-parameter "custom" could be given to indicate a special handling.
        #
        #  List of item-types. Not all are field-types of fields in "fields" and "renderedFields".
        #   - any
        #   - attachment
        #   - comments-page     (not a field)
        #   - component
        #   - date
        #   - datetime
        #   - group
        #   - issuelinks
        #   - issuetype
        #   - number
        #   - option
        #   - option-with-child
        #   - priority
        #   - progress
        #   - project
        #   - resolution
        #   - securitylevel
        #   - status
        #   - string
        #   - timetracking
        #   - user
        #   - version
        #   - votes
        #   - watches
        #   - worklog
        #

        custom_func_map = {
            'com.pyxis.greenhopper.jira:gh-sprint': cls.render_sprint
        }

        if custom and custom in custom_func_map:
            f = custom_func_map[custom]
            return f(value)

        type_func_map = {
            'any': cls.render_none,
            'attachment': cls.render_none,  # TODO: 'issuelinks': cls.render_none
            'component': cls.render_component_or_version_type,
            'date': cls.render_through,
            'datetime': cls.render_datetime_type,
            'group': cls.render_name_attr,
            'issuelinks': cls.render_none,  # TODO: 'issuelinks': cls.render_none
            'issuetype': cls.render_name_attr,
            'number': cls.render_through,
            'option': cls.render_value_attr,
            'option-with-child': cls.render_option_with_child_type,
            'priority': cls.render_name_attr,
            'progress': cls.render_none,  # TODO: 'progress': cls.render_none
            'project': cls.render_project_type,
            'resolution': cls.render_name_attr,
            'securitylevel': cls.render_name_attr,
            'status': cls.render_name_attr,
            'string': cls.render_through,
            'timetracking': cls.render_none,  # TODO: 'timetracking': cls.render_none
            'user': cls.render_user_type,
            'version': cls.render_component_or_version_type,
            'votes': cls.render_votes_type,
            'watches': cls.render_watches_type,
            'worklog': cls.render_none  # TODO: 'worklog': cls.render_none
        }

        try:
            f = type_func_map[field_type]
            return f(value)
        except KeyError:
            cls.log.warning(f"The field-type {field_type} was given, but not renderer is implemented for this type."
                            " The unrendered value is used.")
            return value

    @classmethod
    def render_field(cls, issue, field_id: str):
        """ Render the issue's field identified by the given field-id. The field can be a system-fiels (summary,...),
        or a custom field (customfield_12345).

        Fields can be either a simple type, or an array of simple types.

        Data-types:
            - array
            - A simple type, one of the types listed in Item-types in render_field_type()

        :return The - maybe - rendered field content. If a field has no content or can't be rendered, the return-
                value is an empty string. It is not None because the template would render None to the string "None".
        """

        # The field is absent in case it is hidden in the Field Configuration and the
        # Field Configuration is the only one in the system. In this case field_config is None.
        field_config = cls.cls_api_2_field_as_map[field_id]
        if not field_config:
            cls.log.warning(f"The field_config for the field to be rendered '{field_id}' is not in the list got"
                            " from rest/api/2/field. The field will be rendered as empty string. ")
            return ''

        if cls.is_text_field(field_config):
            try:
                rendered_field_value = issue['renderedFields'][field_id]
                # Most of the text-type-fields won't have links. But to keep the code simple,
                # pipe all text-type-fields through the "URL-replacer".
                html = cls.replace_server_url_with_local_url(rendered_field_value, issue['key'], issue['id'])
                return html
            except KeyError:
                pass

        # It could be both the unrendered as well as the rendered values are null.
        try:
            unrendered_field_value = issue['fields'][field_id]
        except KeyError:
            return ''

        if not unrendered_field_value:
            return ''

        # Now it comes to the rendering...
        if 'items' not in field_config['schema']:
            # Is a "direct" type, not a list.
            return cls.render_field_type(field_config['schema']['type'], unrendered_field_value)
        elif field_config['schema']['type'] == 'array':
            # If 'items' is given, it tells us the data-type, and 'type' tells us the collection type.
            rendered_values = []

            try:
                custom = field_config['schema']['custom']
            except KeyError:
                custom = None

            for item in unrendered_field_value:
                rendered_values.append(cls.render_field_type(field_config['schema']['items'], item, custom))
            # Don't know if the comma is the proper delimiter for all types. But let's start this way.
            return ', '.join(rendered_values)
        else:
            # Is not an array nor a direct type. Don't know how to handle this. Because the unrendered field is
            # plain text it is save to return its value.
            return unrendered_field_value

    @staticmethod
    def is_text_field(field_config):
        if 'schema' in field_config and 'type' in field_config['schema'] and field_config['schema']['type'] == 'string':
            return True
        return False

    @classmethod
    def replace_server_url_with_local_url(cls, html, issue_key, issue_id):
        if not html:
            return ''

        """ Take a HTML and replace all relevant links from the server-URL to the local URL. """
        soup = BeautifulSoup(html, "html.parser")
        img_tags = soup.find_all('img')
        # <IMG>:
        for img_tag in img_tags:
            # I think all IMGs are links to attached images.
            url: str = img_tag['src']
            img_tag['src'] = cls.to_local_attachment_url(issue_key, issue_id, attachment_url=url)
        # <A>:
        a_tags = soup.find_all('a')
        for a_tag in a_tags:
            # a_tag is of type class 'bs4.element.Tag'. The Pythonic "'href' in a_tag" won't work here.
            if not a_tag.has_attr('href'):
                continue
            href: str = a_tag['href']
            # Not all <A>-tags are links to attachments. <A>-tags are used for:
            #   1. Attachment/document
            #   2. External link
            #   3. Mail-to
            #   4. Anchor
            # Only 1) shall be rewritten to a local URL.
            # The pattern for 1) is '/secure/attachment/'
            if href.find('/secure/attachment/') >= 0:
                a_tag['href'] = cls.to_local_attachment_url(issue_key, issue_id, attachment_url=href)
            # The downloadable documents have a superscript <SUP> download-icon after the name. This icon is not part of
            # the export. Remove it.
            for sup in a_tag.find_all('sup'):
                sup.decompose()

        return str(soup)

    @classmethod
    def to_local_attachment_url(cls, issue_key: str, attachment_id: str = None,
                                attachment_filename: str = None, attachment_url: str = None):
        """ Transform the URL to the local downloaded files.

        Attachment-URLs have the format:
            http://localhost:2990/jira/secure/attachment/10300/attachment-A.png
        Note the separated ID and name.

        Images in a text-field have the format:
            /jira/secure/attachment/10300/10300_attachment-A.png
        Note the ID and ID+name.

        This function handles both formats.
        """

        if not (attachment_id and attachment_filename or attachment_url):
            raise ValueError("Need either attachment_id and attachment_filename, or attachment_url. "
                             f"Got: attachment_id {attachment_id}, attachment_filename {attachment_filename}"
                             f", attachment_url {attachment_url}.")

        project_key = issue_key.split('-')[0]
        if attachment_url:
            parts = attachment_url.split('/')
            # attachment_id = parts[-2]
            local_attachment_name = parts[-1]
        else:
            local_attachment_name = f'{attachment_id}_{attachment_filename}'

        url_parts = [
            cls.local_rel_attachment_base_url,
            project_key,
            Tools.calc_bucket(issue_key),
            issue_key,
            local_attachment_name.replace(' ', '+')
        ]
        local_url = '/'.join(url_parts)
        return local_url

    @classmethod
    def to_local_attachment_thumb_url(cls, issue_key, thumbnail_url):
        """ Transform the thumbnail-URL to the local downloaded files.

        Thumbnail-URLs have the form:
            http://localhost:2990/jira/secure/thumbnail/10401/_thumb_10401.png
        """
        project_key = issue_key.split('-')[0]

        thumbnail_name = thumbnail_url.split('/')[-1]
        url_parts = [
            cls.local_rel_attachment_base_url,
            project_key,
            Tools.calc_bucket(issue_key),
            issue_key,
            'thumbs',
            thumbnail_name
        ]
        local_url = '/'.join(url_parts)
        return local_url

    @classmethod
    def filters(cls):
        """ Return render-functions to be exposed as Jinja2-filters. """
        return {
            'render_datetime_type': cls.render_datetime_type,
            'render_user_type': cls.render_user_type,
            'render_field': cls.render_field,
            'replace_server_url_with_local_url': cls.replace_server_url_with_local_url,
            'to_local_attachment_url': cls.to_local_attachment_url,
            'to_local_attachment_thumb_url': cls.to_local_attachment_thumb_url
        }
