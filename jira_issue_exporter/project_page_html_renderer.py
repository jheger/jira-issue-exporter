import pathlib
from dataclasses import dataclass

from base_html_renderer import BaseHtmlRenderer
from tools import Tools


@dataclass
class ProjectPageHtmlRenderer(BaseHtmlRenderer):

    def __post_init__(self):
        super().__post_init__()

    def render(self, project):
        """ Render a project-page, remove comments to reduce page-size, normalize line-endings not to confuse
         editors. """

        project_template = self.jinja_environment.get_template('project_page.html.jinja')
        project_html = project_template.render(api_2_field_as_map=self.api_2_field_as_map, project=project)
        project_html = self.remove_comments_and_beautify(project_html)
        return project_html

    def write(self, html, project_key):
        p = pathlib.Path(self.rendered_html_projects_path, project_key)
        p.mkdir(parents=True, exist_ok=True)
        Tools.write_textfile_with_encoding(pathlib.Path(p, project_key + '.html'), text=html)
