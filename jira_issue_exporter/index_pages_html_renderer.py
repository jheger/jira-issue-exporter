import pathlib
from dataclasses import dataclass, field
from typing import List, Dict

from base_html_renderer import BaseHtmlRenderer
from tools import Tools


@dataclass
class IndexPagesHtmlRenderer(BaseHtmlRenderer):
    export_datetime: str
    current_secondlevel_subindex_page_no: int = field(init=False, default=1)
    total_issue_count: int = 0
    field_ids: list = field(
        default_factory=lambda: 'issuetype,issuekey,summary,reporter,assignee,status,resolution'.split(','))
    field_names: List[str] = field(default_factory=list, init=False)
    issues: List[dict] = field(default_factory=list, init=False)
    issues_links: Dict[str, str] = field(default_factory=dict, init=False)
    current_issue_count: int = field(default=0, init=False)
    rendered_subindex_pages: int = 0
    subindex_page_names: List[str] = field(default_factory=list, init=False)
    page_min_index: int = field(default=1, init=False)
    MIN_ISSUES_PER_SUBINDEX_PAGE = 10

    def __post_init__(self):
        super().__post_init__()

        # Remove spaces from field-ids.
        for i, field_id in enumerate(self.field_ids):
            self.field_ids[i] = field_id.replace(' ', '')

        for field_id in self.field_ids:
            if field_id in self.api_2_field_as_map:
                self.field_names.append(self.api_2_field_as_map[field_id]['name'])
            else:
                raise ValueError(f"Field-ID '{field_id}' unknown.")

    def add_issue(self, issue_json, bucket=None):
        self.issues.append(issue_json)

        project_key, issue_key, issue_id = Tools.get_issue_info(issue_json)
        self.issues_links[issue_key] = '/'.join(['projects',
                                                 project_key,
                                                 bucket if bucket else Tools.calc_bucket(issue_key),
                                                 issue_key + '.html'])

        self.current_issue_count += 1

    def render_and_write_subindex_page(self):
        subindex_page_name = self.build_subindex_page_name(min_index=self.page_min_index,
                                                           max_index=self.current_issue_count)
        self.log.debug(f": {subindex_page_name}")
        subindex_page_template = self.jinja_environment.get_template('subindex_page.html.jinja')
        page_html = subindex_page_template.render(api_2_field_as_map=self.api_2_field_as_map,
                                                  page_min_index=self.page_min_index,
                                                  page_max_index=self.current_issue_count,
                                                  overall_issue_count=self.total_issue_count,
                                                  field_ids=self.field_ids,
                                                  field_names=self.field_names,
                                                  issues=self.issues,
                                                  issue_links=self.issues_links)
        self.page_min_index = self.current_issue_count + 1
        self.rendered_subindex_pages += 1

        self.rendered_html_path.mkdir(parents=True, exist_ok=True)
        page_html = self.remove_comments_and_beautify(page_html)
        self.subindex_page_names.append(subindex_page_name)
        Tools.write_textfile_with_encoding(pathlib.Path(self.rendered_html_path, subindex_page_name),
                                           text=page_html)
        self.issues.clear()

    def build_subindex_page_name(self, min_index, max_index):
        if self.total_issue_count >= 0:
            length = len(str(self.total_issue_count))
        else:
            # Both 0 and 1 are OK.
            length = 0
        s = f"index_{min_index:0{length}}-{max_index:0{length}}.html"
        return s

    def render_and_write_topindex_page(self):
        topindex_page_template = self.jinja_environment.get_template('topindex_page.html.jinja')
        page_html = topindex_page_template.render(subindex_page_names=self.subindex_page_names,
                                                  export_date=self.export_datetime)
        page_html = self.remove_comments_and_beautify(page_html)
        Tools.write_textfile_with_encoding(pathlib.Path(self.rendered_html_path, '_index.html'), text=page_html)
