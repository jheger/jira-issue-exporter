import os
import pathlib
import shutil
from dataclasses import dataclass
from logging import Logger
from typing import Any

from bs4 import BeautifulSoup, Comment
from jinja2 import Environment, FileSystemLoader

from field_html_renderer import FieldHtmlRenderer


@dataclass
class BaseHtmlRenderer:
    log: Logger
    export_path: str
    api_2_field_as_map: dict
    filters: Any

    cls_api_2_field_as_map = {}

    def __post_init__(self):
        BaseHtmlRenderer.cls_api_2_field_as_map = self.api_2_field_as_map

        self.rendered_html_path = pathlib.Path(self.export_path, 'rendered', 'html')
        self.rendered_html_projects_path = pathlib.Path(self.rendered_html_path, 'projects')
        self.rendered_html_static_path = pathlib.Path(self.rendered_html_path, 'static')

        # Get the directory of this class. This is to find the templates-folder.
        me = os.path.dirname(os.path.realpath(__file__))
        self.jinja_environment = Environment(
            loader=FileSystemLoader(pathlib.Path(me, 'templates'))
        )

        self.jinja_environment.filters.update(self.filters())

    def copy_static_html_content_to_export_dir(self):
        cwd = os.path.dirname(os.path.realpath(__file__))
        # TODO How to copy the content, not the individual files?
        self.rendered_html_static_path.mkdir(parents=True, exist_ok=True)
        shutil.copy(pathlib.Path(cwd, 'static', 'style.css'), self.rendered_html_static_path)
        shutil.copy(pathlib.Path(cwd, 'static', 'attachment-icon.png'), self.rendered_html_static_path)
        return '../../../static'

    @staticmethod
    def remove_comments_and_beautify(html):
        """ Remove all HTML-comments to reduce the page-size.
        After that, the page has been reformatted by BeautifulSoup. All tags are aligend to the first column.
        This is a side-effect, it is not indented. To reformat it a little bit, .rettify() is called.
        """
        soup = BeautifulSoup(html, 'html.parser')
        for comments in soup.findAll(text=lambda text: isinstance(text, Comment)):
            comments.extract()

        html = soup.prettify()
        # Normalize line-endings. It turned out the result until now could be a mixture of line-ending-styles.
        # Viewing the HTML in editors could result in displaying special-chars or placeholders for line-endings.
        # Avoid that.
        html = '\n'.join(html.splitlines())

        return html
